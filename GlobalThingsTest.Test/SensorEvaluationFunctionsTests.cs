using Xunit;
using GlobalThingsTest.Worker.Implementations;
using GlobalThingsTest.Worker.Models;
using System.Collections.Generic;
using System;

namespace GlobalThingsTest.Test;

public class SensorEvaluationFunctionsTests
{
    [Theory]
    [MemberData(nameof(GetDataTest1))]
    public void AreLastFiveMeasuresInsideInterval_ShouldWork(List<Medicao> medicoes, DateTimeOffset dataHoraReferencia)
    {
        var sensorEvaluationFunctions = new SensorEvaluationFunctions();

        var result = sensorEvaluationFunctions.AreLastSixMeasuresInsideInterval(medicoes, dataHoraReferencia);

        Assert.True(result);
    }

    [Theory]
    [MemberData(nameof(GetDataTest2))]
    public void AreLastFiveMeasuresInsideInterval_ShouldFail(List<Medicao> medicoes, DateTimeOffset dataHoraReferencia)
    {
        var sensorEvaluationFunctions = new SensorEvaluationFunctions();

        var result = sensorEvaluationFunctions.AreLastSixMeasuresInsideInterval(medicoes, dataHoraReferencia);

        Assert.False(result);
    }

    [Theory]
    [MemberData(nameof(GetDataTest3))]
    public void IsAverageInsideInterval_ShouldWork(List<Medicao> medicoes, DateTimeOffset dataHoraReferencia)
    {
        var sensorEvaluationFunctions = new SensorEvaluationFunctions();

        var result = sensorEvaluationFunctions.IsAverageInsideInterval(medicoes, dataHoraReferencia);

        Assert.True(result);
    }

    [Theory]
    [MemberData(nameof(GetDataTest4))]
    public void IsAverageInsideInterval_ShouldFail(List<Medicao> medicoes, DateTimeOffset dataHoraReferencia)
    {
        var sensorEvaluationFunctions = new SensorEvaluationFunctions();

        var result = sensorEvaluationFunctions.IsAverageInsideInterval(medicoes, dataHoraReferencia);

        Assert.False(result);
    }

    public IEnumerable<object[]> GetDataTest1()
    {
        var allData = new List<object[]>
        {
            new object[] { // Testando o Where(x => x.DataHora <= dataHoraRef)
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T12:00:00"), Decimal.MinusOne),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("0.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("0.5")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("0.7")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T17:00:00"), Decimal.Parse("0.9")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T18:00:00"), Decimal.Parse("1.1"))
                },
                DateTimeOffset.Parse("2021-03-17T17:00:00")
            },
            new object[] { // Testando o OrderByDescending(x => x.DataHora)
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("0.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T11:00:00"), Decimal.MaxValue),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T18:00:00"), Decimal.Parse("0.001")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("0.5")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("0.7")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T12:00:00"), Decimal.MaxValue),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T17:00:00"), Decimal.Parse("0.9"))
                },
                DateTimeOffset.Parse("2021-03-17T18:00:00")
            },
            new object[] { // Testando o Take(6)
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T12:00:00"), Decimal.MaxValue),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.MinusOne),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("0.9")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("0.7")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("0.5")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T17:00:00"), Decimal.Parse("0.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T18:00:00"), Decimal.Parse("0.1"))
                },
                DateTimeOffset.Parse("2021-03-17T18:00:00")
            },
            new object[] { // Testando valores dos dois lados do intervalo
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.MinusOne),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("50.9")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("0.7")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("159.5")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T17:00:00"), Decimal.MinValue),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T18:00:00"), Decimal.MaxValue)
                },
                DateTimeOffset.Parse("2021-03-17T18:00:00")
            }
        };

        return allData;
    }

    public IEnumerable<object[]> GetDataTest2()
    {
        var allData = new List<object[]>
        {
            new object[] { // Caso onde nao tenham 6 medicoes
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("0.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("0.5")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("0.7"))
                },
                DateTimeOffset.Parse("2021-03-17T16:00:00")
            },
            new object[] { // Caso onde nao tenham 6 medicoes abaixo da dataHoraReferencia
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("0.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T18:00:00"), Decimal.Parse("1.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("0.5")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("0.7")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T17:00:00"), Decimal.Parse("0.9"))
                },
                DateTimeOffset.Parse("2021-03-17T14:00:00")
            },
            new object[] { // Caso onde os valores estao fora do intervalo da regra
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T17:00:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T18:00:00"), Decimal.Parse("6.3"))
                },
                DateTimeOffset.Parse("2021-03-17T18:00:00")
            },
            new object[] { // Caso onde apenas um valor esta dentro do intervalo da regra
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T17:00:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T18:00:00"), Decimal.Parse("6.3"))
                },
                DateTimeOffset.Parse("2021-03-17T18:00:00")
            }
        };

        return allData;
    }
    
    public IEnumerable<object[]> GetDataTest3()
    {
        var allData = new List<object[]>
        {
            new object[] { // Caso onde a média está dentro do intervalo da regra (abaixo de 3)
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:01:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:02:00"), Decimal.Parse("0.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:03:00"), Decimal.Parse("0.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:04:00"), Decimal.Parse("0.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:05:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:06:00"), Decimal.Parse("0.0")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:07:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:08:00"), Decimal.Parse("0.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:09:00"), Decimal.Parse("0.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:10:00"), Decimal.Parse("0.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:11:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:12:00"), Decimal.Parse("0.0")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:13:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:14:00"), Decimal.Parse("0.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:15:00"), Decimal.Parse("0.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:16:00"), Decimal.Parse("0.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:17:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:18:00"), Decimal.Parse("0.0")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:19:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:20:00"), Decimal.Parse("0.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:21:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:22:00"), Decimal.Parse("0.0")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:23:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:24:00"), Decimal.Parse("0.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:25:00"), Decimal.Parse("0.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:26:00"), Decimal.Parse("0.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:27:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:28:00"), Decimal.MinusOne),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:29:00"), Decimal.Parse("1.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:30:00"), Decimal.Parse("1.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:31:00"), Decimal.Parse("1.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:32:00"), Decimal.Parse("1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:33:00"), Decimal.Parse("1.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:34:00"), Decimal.Parse("1.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:35:00"), Decimal.Parse("1.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:36:00"), Decimal.Parse("1.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:37:00"), Decimal.Parse("1.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:38:00"), Decimal.MinusOne),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:39:00"), Decimal.Parse("2.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:40:00"), Decimal.Parse("2.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:41:00"), Decimal.Parse("2.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:42:00"), Decimal.Parse("2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:43:00"), Decimal.Parse("2.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:44:00"), Decimal.Parse("2.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:45:00"), Decimal.Parse("2.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:46:00"), Decimal.Parse("2.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:47:00"), Decimal.Parse("2.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:48:00"), Decimal.Parse("2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:49:00"), Decimal.Parse("2.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:50:00"), Decimal.Parse("2.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:51:00"), Decimal.Parse("2.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:52:00"), Decimal.Parse("2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:53:00"), Decimal.Parse("2.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:54:00"), Decimal.Parse("2.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:55:00"), Decimal.Parse("2.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:56:00"), Decimal.Parse("2.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:57:00"), Decimal.Parse("2.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:58:00"), Decimal.Parse("2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:59:00"), Decimal.Parse("2.1"))
                },
                DateTimeOffset.Parse("2021-03-17T13:54:00")
            },
            new object[] { // Caso onde a média está dentro do intervalo da regra (acima de 48)
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:01:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:02:00"), Decimal.Parse("48.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:03:00"), Decimal.Parse("48.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:04:00"), Decimal.Parse("48.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:05:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:06:00"), Decimal.Parse("48.0")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:07:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:08:00"), Decimal.Parse("48.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:09:00"), Decimal.Parse("48.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:10:00"), Decimal.Parse("48.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:11:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:12:00"), Decimal.Parse("48.0")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:13:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:14:00"), Decimal.Parse("48.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:15:00"), Decimal.Parse("48.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:16:00"), Decimal.Parse("48.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:17:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:18:00"), Decimal.Parse("48.0")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:19:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:20:00"), Decimal.Parse("48.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:21:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:22:00"), Decimal.Parse("48.0")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:23:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:24:00"), Decimal.Parse("48.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:25:00"), Decimal.Parse("47.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:26:00"), Decimal.Parse("48.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:27:00"), Decimal.Parse("48.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:28:00"), Decimal.Parse("50")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:29:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:30:00"), Decimal.Parse("49.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:31:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:32:00"), Decimal.Parse("49")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:33:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:34:00"), Decimal.Parse("49.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:35:00"), Decimal.Parse("49.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:36:00"), Decimal.Parse("49.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:37:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:38:00"), Decimal.Parse("50")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:39:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:40:00"), Decimal.Parse("50.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:41:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:42:00"), Decimal.Parse("50")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:43:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:44:00"), Decimal.Parse("50.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:45:00"), Decimal.Parse("50.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:46:00"), Decimal.Parse("50.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:47:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:48:00"), Decimal.Parse("50")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:49:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:50:00"), Decimal.Parse("50.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:51:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:52:00"), Decimal.Parse("50")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:53:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:54:00"), Decimal.Parse("50.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:55:00"), Decimal.Parse("50.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:56:00"), Decimal.Parse("50.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:57:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:58:00"), Decimal.Parse("50")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:59:00"), Decimal.Parse("50.1"))
                },
                DateTimeOffset.Parse("2021-03-17T13:54:00")
            }
        };

        return allData;
    }
    public IEnumerable<object[]> GetDataTest4()
    {
        var allData = new List<object[]>
        {
            new object[] { // Caso onde nao tenham 50 medicoes
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("0.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("0.5")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("0.7"))
                },
                DateTimeOffset.Parse("2021-03-17T16:00:00")
            },
            new object[] { // Caso onde nao tenham 50 medicoes abaixo da dataHoraReferencia
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:00:00"), Decimal.Parse("0.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T14:00:00"), Decimal.Parse("0.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T18:00:00"), Decimal.Parse("1.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T15:00:00"), Decimal.Parse("0.5")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T16:00:00"), Decimal.Parse("0.7")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T17:00:00"), Decimal.Parse("0.9"))
                },
                DateTimeOffset.Parse("2021-03-17T14:00:00")
            },
            new object[] { // Caso onde a média está fora do intervalo da regra
                new List<Medicao>
                {
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:01:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:02:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:03:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:04:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:05:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:06:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:07:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:08:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:09:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:10:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:11:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:12:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:13:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:14:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:15:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:16:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:17:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:18:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:19:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:20:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:21:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:22:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:23:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:24:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:25:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:26:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:27:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:28:00"), Decimal.MinusOne),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:29:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:30:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:31:00"), Decimal.Parse("50.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:32:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:33:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:34:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:35:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:36:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:37:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:38:00"), Decimal.MinusOne),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:39:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:40:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:41:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:42:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:43:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:44:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:45:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:46:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:47:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:48:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:49:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:50:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:51:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:52:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:53:00"), Decimal.Parse("33.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:54:00"), Decimal.Parse("25.4")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:55:00"), Decimal.Parse("18.2")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:56:00"), Decimal.Parse("25.27")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:57:00"), Decimal.Parse("49.1")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:58:00"), Decimal.Parse("6.3")),
                    new Medicao(DateTimeOffset.Parse("2021-03-17T13:59:00"), Decimal.Parse("33.1"))
                },
                DateTimeOffset.Parse("2021-03-17T13:54:00")
            }
        };

        return allData;
    }
}