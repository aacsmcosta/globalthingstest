namespace GlobalThingsTest.Api.Configurations;

public class ApiDatabaseConfiguration
{
    public string ConnectionString { get; set; } = null!;

    public string DatabaseName { get; set; } = null!;

    public string DadosSensorCollectionName { get; set; } = null!;

    public string SetorEquipamentoCollectionName { get; set; } = null!;
}
