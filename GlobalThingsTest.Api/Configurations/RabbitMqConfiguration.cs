namespace GlobalThingsTest.Api.Configurations;

public class RabbitMqConfiguration
{
    public string Host { get; set; } = null!;
    public string Queue { get; set; } = null!;
}