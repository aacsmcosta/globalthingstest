using GlobalThingsTest.Api.Models;
using Microsoft.AspNetCore.Mvc;

public interface IDadosSensorBusiness
{
    public Task<ActionResult<DadosSensor>> FindDadosSensor(Int32 id);

    public Task<List<DadosSensor>> FindDadosSensor();
    
    public Task<IActionResult> ArmazenarDadosSensor(DadosSensor dadosSensor);

    public DadosSensorDTO CreateDadosSensorDTO(Int32 idSensor, List<Medicao> medicoesList);

    public Task<DadosSensor> GetDadosSensor(Int32 id);
}