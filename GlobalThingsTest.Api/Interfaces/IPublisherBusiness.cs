using GlobalThingsTest.Api.Models;

public interface IPublisherBusiness
{
    public void PublishMessage(DadosSensor sensor);
}