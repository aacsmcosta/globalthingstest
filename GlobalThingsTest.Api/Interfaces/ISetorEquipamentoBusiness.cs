using GlobalThingsTest.Api.Models;
using Microsoft.AspNetCore.Mvc;

public interface ISetorEquipamentoBusiness
{
    public Task<IActionResult> VincularSetorEquipamentoSensor(SetorEquipamentoSensorRequest setorEquipamentoSensor);

    public Task<List<DadosSensorDTO>> FindMedicoesByIdSetorEquipamento(Int32 IdSetorEquipamento);
}