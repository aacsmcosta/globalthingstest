
using Microsoft.AspNetCore.Mvc;
using GlobalThingsTest.Api.Models;

namespace GlobalThingsTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DadosSensorController : ControllerBase
    {
        private readonly IDadosSensorBusiness _dadosSensorBusiness;

        public DadosSensorController(IDadosSensorBusiness dadosSensorBusiness)
        {
            this._dadosSensorBusiness = dadosSensorBusiness;
        }

        [HttpGet]
        public async Task<List<DadosSensor>> GetDadosSensor()
        {
            var response = await _dadosSensorBusiness.FindDadosSensor();

            return response;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DadosSensor>> GetDadosSensor(Int32 id)
        {
            var response = await _dadosSensorBusiness.FindDadosSensor(id);

            return response;
        }

        [HttpPost]
        public async Task<IActionResult> PostDadosSensor([FromBody] DadosSensor dadosSensor)
        {
            var response = await _dadosSensorBusiness.ArmazenarDadosSensor(dadosSensor);
            
            return response;
        }

    }
}
