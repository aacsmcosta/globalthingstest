using Microsoft.AspNetCore.Mvc;
using GlobalThingsTest.Api.Models;

namespace GlobalThingsTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SetorEquipamentoController : ControllerBase
    {
        private readonly ISetorEquipamentoBusiness _setorEquipamentoBusiness;

        public SetorEquipamentoController(ISetorEquipamentoBusiness setorEquipamentoBusiness)
        {
            this._setorEquipamentoBusiness = setorEquipamentoBusiness;
        }

        [HttpGet("{idSetorEquipamento}")]
        public async Task<List<DadosSensorDTO>> GetMedicoesByIdSetorEquipamento(Int32 idSetorEquipamento) {

            var response = await _setorEquipamentoBusiness.FindMedicoesByIdSetorEquipamento(idSetorEquipamento);

            return response;
        }

        [HttpPost]
        public async Task<IActionResult> VincularSetorEquipamentoSensor([FromBody] SetorEquipamentoSensorRequest setorEquipamentoSensor) {

            var response = await _setorEquipamentoBusiness.VincularSetorEquipamentoSensor(setorEquipamentoSensor);

            return response;
        }
    }
}