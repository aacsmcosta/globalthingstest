using GlobalThingsTest.Api.Models;
using GlobalThingsTest.Api.Business;
using GlobalThingsTest.Api.Services;
using GlobalThingsTest.Api.Configurations;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.Configure<ApiDatabaseConfiguration>(
    builder.Configuration.GetSection("ApiDatabase"));
builder.Services.Configure<RabbitMqConfiguration>(
    builder.Configuration.GetSection("RabbitMQConfig"));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IDadosSensorBusiness, DadosSensorBusiness>();
builder.Services.AddScoped<ISetorEquipamentoBusiness, SetorEquipamentoBusiness>();
builder.Services.AddScoped<IPublisherBusiness, PublisherBusiness>();
builder.Services.AddSingleton<DadosSensorService>();
builder.Services.AddSingleton<SetorEquipamentoService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    // app.UseSwagger();
    // app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
