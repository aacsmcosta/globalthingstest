using GlobalThingsTest.Api.Models;
using GlobalThingsTest.Api.Configurations;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.Core.Events;

namespace GlobalThingsTest.Api.Services;

public class DadosSensorService
{
    private readonly IMongoCollection<DadosSensor> _dadosSensorCollection;

    public DadosSensorService(IOptions<ApiDatabaseConfiguration> apiDatabaseSettings)
    {
        var mongoClient = new MongoClient(apiDatabaseSettings.Value.ConnectionString);

        var mongoDatabase = mongoClient.GetDatabase(
            apiDatabaseSettings.Value.DatabaseName);

        _dadosSensorCollection = mongoDatabase.GetCollection<DadosSensor>(
            apiDatabaseSettings.Value.DadosSensorCollectionName);
    }

    public async Task<List<DadosSensor>> GetAsync() =>
        await _dadosSensorCollection.Find(_ => true).ToListAsync();

    public async Task<DadosSensor?> GetAsync(Int32 id) =>
        await _dadosSensorCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task CreateAsync(DadosSensor newDadosSensor) =>
        await _dadosSensorCollection.InsertOneAsync(newDadosSensor);

    public async Task UpdateAsync(Int32 id, DadosSensor updatedDadosSensor) =>
        await _dadosSensorCollection.ReplaceOneAsync(x => x.Id == id, updatedDadosSensor);

    public async Task RemoveAsync(Int32 id) =>
        await _dadosSensorCollection.DeleteOneAsync(x => x.Id == id);
}