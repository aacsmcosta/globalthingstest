using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Text.Json.Serialization;

namespace GlobalThingsTest.Api.Models
{
    public class SetorEquipamentoSensor {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonIgnore]
        public string? _id { get; set; }
        
        public Int32 IdSetorEquipamento { get; set; }

        public List<Int32> IdSensores { get; set; }

        public SetorEquipamentoSensor(Int32 idSetorEquipamento, List<Int32> idSensores) {
            this.IdSetorEquipamento = idSetorEquipamento;
            this.IdSensores = idSensores;
        }

    }
}