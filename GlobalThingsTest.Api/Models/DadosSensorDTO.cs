namespace GlobalThingsTest.Api.Models
{
    public class DadosSensorDTO {
        public Int32 Id { get; set; }

        public List<Medicao> Medicoes { get; set; }

        public DadosSensorDTO(Int32 id, List<Medicao> medicoes) {
            this.Id = id;
            this.Medicoes = medicoes;
        }

    }
}