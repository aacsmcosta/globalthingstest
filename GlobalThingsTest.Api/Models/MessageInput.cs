namespace GlobalThingsTest.Api.Models
{
    public class MessageInput
    {
        public Int32 Id { get; set; }
        public DateTimeOffset DataHora { get; set; }
        public Decimal ValorMedido { get; set; }

        public MessageInput(Int32 id, DateTimeOffset dataHora, Decimal valorMedido)
        {
            Id = id;
            DataHora = dataHora;
            ValorMedido = valorMedido;
        }
    }
}
