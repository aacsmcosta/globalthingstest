using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Text.Json.Serialization;

namespace GlobalThingsTest.Api.Models
{
    public class DadosSensor {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonIgnore]
        public string? _id { get; set; }

        public Int32 Id { get; set; }

        public String Codigo { get; set; }

        public List<Medicao> Medicoes { get; set; }

        public DadosSensor(Int32 id, String codigo, List<Medicao> medicoes) {
            this.Id = id;
            this.Codigo = codigo;
            this.Medicoes = medicoes;
        }

    }
}