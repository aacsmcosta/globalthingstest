namespace GlobalThingsTest.Api.Models
{
    public class Medicao {
        
        public DateTimeOffset DataHora { get; set; }

        public Decimal ValorMedido { get; set; }

        public Medicao(DateTimeOffset dataHora, Decimal valorMedido) {
            this.DataHora = dataHora;
            this.ValorMedido = valorMedido;
        }
    }
}