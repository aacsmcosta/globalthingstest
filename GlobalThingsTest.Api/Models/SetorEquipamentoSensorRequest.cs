namespace GlobalThingsTest.Api.Models
{
    public class SetorEquipamentoSensorRequest {
        
        public Int32 IdSetorEquipamento { get; set; }

        public Int32 IdSensor { get; set; }

        public SetorEquipamentoSensorRequest(Int32 idSensor, Int32 idSetorEquipamento) {
            this.IdSetorEquipamento = idSetorEquipamento;
            this.IdSensor = idSensor;
        }

    }
}