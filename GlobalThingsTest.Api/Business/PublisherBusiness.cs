using GlobalThingsTest.Api.Models;
using GlobalThingsTest.Api.Configurations;
using RabbitMQ.Client;
using Microsoft.Extensions.Options;
using System.Text;
using System.Text.Json;

namespace GlobalThingsTest.Api.Business;

public class PublisherBusiness : IPublisherBusiness
{
    private readonly RabbitMqConfiguration _configuration;
    private readonly IConnection _connection;
    private readonly IModel _channel;

    public PublisherBusiness(IOptions<RabbitMqConfiguration> options)
    {
        _configuration = options.Value;
        var factory = new ConnectionFactory
        {
            HostName = _configuration.Host
        };

        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        _channel.QueueDeclare(
            queue: _configuration.Queue,
            durable: false,
            exclusive: false,
            autoDelete: false,
            arguments: null
        );
    }

    public void PublishMessage(DadosSensor sensor)
    {
        foreach(var medicao in sensor.Medicoes)
        {
            var message = new MessageInput(sensor.Id, medicao.DataHora, medicao.ValorMedido);

            var jsonString = JsonSerializer.Serialize(message);
            var bytesMessage = Encoding.UTF8.GetBytes(jsonString);

            _channel.BasicPublish(
                exchange: "",
                routingKey: _configuration.Queue,
                basicProperties: null,
                body: bytesMessage
            );

            Console.WriteLine(" [x] Sent {0}", jsonString);
        }

    }
}
