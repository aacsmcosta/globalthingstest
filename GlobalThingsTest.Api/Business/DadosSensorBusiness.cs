using GlobalThingsTest.Api.Models;
using GlobalThingsTest.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace GlobalThingsTest.Api.Business
{
    public class DadosSensorBusiness : IDadosSensorBusiness {
        private readonly DadosSensorService _dadosSensorService;

        private readonly IPublisherBusiness _publisherBusiness;

        public DadosSensorBusiness(DadosSensorService dadosSensorService, IPublisherBusiness publisherBusiness)
        {
            _dadosSensorService = dadosSensorService;
            _publisherBusiness = publisherBusiness;
        }

        public async Task<List<DadosSensor>> FindDadosSensor() {
            var sensors = await _dadosSensorService.GetAsync();
            return sensors;
        }

        public async Task<ActionResult<DadosSensor>> FindDadosSensor(Int32 id) {
            var sensor = await GetDadosSensor(id);

            if (sensor is null) {
                return new NotFoundResult();
            }

            return sensor;
        }

        public async Task<IActionResult> ArmazenarDadosSensor(DadosSensor dadosSensor) {
            var selectedDadosSensor = await _dadosSensorService.GetAsync(dadosSensor.Id);
            
            if (selectedDadosSensor != null) {
                SaveRangeMedicao(selectedDadosSensor, dadosSensor.Medicoes);
            }
            else {
                SaveDadosSensor(dadosSensor);
            }

            _publisherBusiness.PublishMessage(dadosSensor);

            return new OkResult();
        }

        public DadosSensorDTO CreateDadosSensorDTO(Int32 idSensor, List<Medicao> medicoesList) {
            return new DadosSensorDTO(idSensor, medicoesList);
        }

        public async Task<DadosSensor> GetDadosSensor(Int32 id) {
            var sensor = await _dadosSensorService.GetAsync(id);
            return sensor;
        }

        private async void SaveDadosSensor(DadosSensor dadosSensor) {
            await _dadosSensorService.CreateAsync(dadosSensor);
        }

        private async void SaveRangeMedicao(DadosSensor dadosSensor, List<Medicao> medicaoList) {
            dadosSensor.Medicoes.AddRange(medicaoList);
            await _dadosSensorService.UpdateAsync(dadosSensor.Id, dadosSensor);
        }

    }
}