using GlobalThingsTest.Api.Models;
using GlobalThingsTest.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace GlobalThingsTest.Api.Business
{
    public class SetorEquipamentoBusiness : ISetorEquipamentoBusiness
    {
        private readonly SetorEquipamentoService _setorEquipamentoService;

        private readonly IDadosSensorBusiness _dadosSensorBusiness;

        public SetorEquipamentoBusiness(SetorEquipamentoService setorEquipamentoService, IDadosSensorBusiness dadosSensorBusiness)
        {
            _setorEquipamentoService = setorEquipamentoService;
            _dadosSensorBusiness = dadosSensorBusiness;
        }

        public async Task<IActionResult> VincularSetorEquipamentoSensor(SetorEquipamentoSensorRequest setorEquipamentoSensor) {

            var selectedSetorEquipamento = await _setorEquipamentoService.GetAsync(setorEquipamentoSensor.IdSetorEquipamento);

            if (selectedSetorEquipamento != null) {
                SaveIdSensor(selectedSetorEquipamento, setorEquipamentoSensor.IdSensor);
                return new OkResult();
            }
            else {
                var newSetorEquipamentoSensor = CreateSetorEquipamentoSensor(setorEquipamentoSensor);
                SaveSetorEquipamentoSensor(newSetorEquipamentoSensor);
                return new OkResult();
            }
        }

        public async Task<List<DadosSensorDTO>> FindMedicoesByIdSetorEquipamento(Int32 IdSetorEquipamento) {
            var setorEquipamentoSensors = await _setorEquipamentoService.GetAsync(IdSetorEquipamento);
            var resultList = new List<DadosSensorDTO>();

            if (setorEquipamentoSensors != null) {
                var idSensores = setorEquipamentoSensors.IdSensores;

                foreach(var idSensor in idSensores) {
                    var sensor = await _dadosSensorBusiness.GetDadosSensor(idSensor);

                    if (sensor != null) {
                        var medicoesList = sensor.Medicoes.OrderByDescending(x => x.DataHora).Take(10).OrderBy(x => x.DataHora).ToList();

                        var sensorMedicoes = _dadosSensorBusiness.CreateDadosSensorDTO(idSensor, medicoesList);

                        resultList.Add(sensorMedicoes);
                    }
                }
            }

            return resultList;
        }

        private SetorEquipamentoSensor CreateSetorEquipamentoSensor(SetorEquipamentoSensorRequest setorEquipamentoSensor) {
            var idSensorList = new List<Int32>();
            idSensorList.Add(setorEquipamentoSensor.IdSensor);
            
            return new SetorEquipamentoSensor(setorEquipamentoSensor.IdSetorEquipamento, idSensorList);
        }

        private async void SaveSetorEquipamentoSensor(SetorEquipamentoSensor setorEquipamentoSensor) {
            await _setorEquipamentoService.CreateAsync(setorEquipamentoSensor);
        }

        private async void SaveIdSensor(SetorEquipamentoSensor setorEquipamentoSensor, Int32 idSensor) {
            setorEquipamentoSensor.IdSensores.Add(idSensor);
            await _setorEquipamentoService.UpdateAsync(setorEquipamentoSensor.IdSetorEquipamento, setorEquipamentoSensor);
        }

    }
}