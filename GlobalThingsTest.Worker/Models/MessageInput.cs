namespace GlobalThingsTest.Worker.Models;

public class MessageInput
{
    public Int32 Id { get; set; }
    public DateTimeOffset DataHora { get; set; }
    public Decimal ValorMedido { get; set; }
}