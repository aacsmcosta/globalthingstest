using GlobalThingsTest.Worker.Models;
using GlobalThingsTest.Worker.Interfaces;
using GlobalThingsTest.Worker.Configurations;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Microsoft.Extensions.Options;
using System.Text;
using System.Text.Json;

namespace GlobalThingsTest.Worker.Implementations;

public class Worker : BackgroundService
{
    private readonly ISensorEvaluation _sensorEvaluation;
    private readonly RabbitMqConfiguration _configuration;
    private readonly IConnection _connection;
    private readonly IModel _channel;

    public Worker(IOptions<RabbitMqConfiguration> options, ISensorEvaluation sensorEvalution)
    {
        _sensorEvaluation = sensorEvalution;

        _configuration = options.Value;
        var factory = new ConnectionFactory
        {
            HostName = _configuration.Host
        };

        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        _channel.QueueDeclare(
            queue: _configuration.Queue,
            durable: false,
            exclusive: false,
            autoDelete: false,
            arguments: null
        );
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var consumer = new EventingBasicConsumer(_channel);
            
        consumer.Received += ConsumerReceived;

        while (!stoppingToken.IsCancellationRequested)
        {
            _channel.BasicConsume(_configuration.Queue, false, consumer);

            await Task.Delay(1000, stoppingToken);
        }

        await Task.CompletedTask;
    }

    public override async Task StopAsync(CancellationToken stoppingToken)
    {
        await base.StopAsync(stoppingToken);
    }

    private void ConsumerReceived(object sender, BasicDeliverEventArgs eventArgs)
    {
            var contentArray = eventArgs.Body.ToArray();
            var contentStr = Encoding.UTF8.GetString(contentArray);
            var message = JsonSerializer.Deserialize<MessageInput>(contentStr);

            _sensorEvaluation.CheckConditions(message);

            _channel.BasicAck(eventArgs.DeliveryTag, false);
    }
}
