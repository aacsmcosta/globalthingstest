using GlobalThingsTest.Worker.Models;
using GlobalThingsTest.Worker.Interfaces;
using GlobalThingsTest.Worker.Services;

namespace GlobalThingsTest.Worker.Implementations;

public class SensorEvaluation : SensorEvaluationFunctions, ISensorEvaluation
{
    private readonly DadosSensorService _dadosSensorService;
    private readonly SetorEquipamentoService _setorEquipamentoService;
    private readonly IEmailClient _emailClient;


    public SensorEvaluation(DadosSensorService dadosSensorService, SetorEquipamentoService setorEquipamentoService, IEmailClient emailClient)
    {
        _dadosSensorService = dadosSensorService;
        _setorEquipamentoService = setorEquipamentoService;
        _emailClient = emailClient;
    }

    public async void CheckConditions(MessageInput input)
    {
        if (input != null)
        {
            var sensor = await _dadosSensorService.GetAsync(input.Id);

            if (sensor != null)
            {
                CheckIfRepeatedValueFiveTimes(input, sensor);
                CheckIfAverageValueIsInsideInterval(input, sensor);    
            }
        }
    }
    
    public void CheckIfRepeatedValueFiveTimes(MessageInput input, DadosSensor sensor)
    {
        var isValueRepeatedFiveTimes = AreLastSixMeasuresInsideInterval(sensor.Medicoes, input.DataHora);

        if (isValueRepeatedFiveTimes)
        {
            _emailClient.SendEmailSensorTemplate(sensor);
        }
    }

    public async void CheckIfAverageValueIsInsideInterval(MessageInput input, DadosSensor sensor)
    {
        var isAverageInsideInterval = IsAverageInsideInterval(sensor.Medicoes, input.DataHora);

        if (isAverageInsideInterval)
        {
            SetorEquipamentoSensor setorEquipamento = await FindSetorEquipamentoBySensorId(sensor);

            if (setorEquipamento != null)
            {
                _emailClient.SendEmailSetorEquipamentoTemplate(setorEquipamento);
            }
            else
            {
                _emailClient.SendEmailSensorTemplate(sensor);
            }
        }
    }

    public async Task<SetorEquipamentoSensor> FindSetorEquipamentoBySensorId(DadosSensor sensor)
    {
        // TODO: Buscar todos os SetorEquipamento no banco GetAsync()
        var setorEquipamentoList = await _setorEquipamentoService.GetAsync();

        foreach(var setorEquipamento in setorEquipamentoList)
        {
            if (setorEquipamento.IdSensores.Contains(sensor.Id))
            {
                return setorEquipamento;
            }
        }

        return null;
    }

}