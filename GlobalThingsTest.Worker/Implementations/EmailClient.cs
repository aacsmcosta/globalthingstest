using GlobalThingsTest.Worker.Interfaces;
using GlobalThingsTest.Worker.Models;
using GlobalThingsTest.Worker.Configurations;
using Microsoft.Extensions.Options;
using System.Net.Mail;

namespace GlobalThingsTest.Worker.Implementations;

public class EmailClient : IEmailClient
{
    private readonly EmailConfiguration _config;
    private readonly SmtpClient _smtpClient;


    public EmailClient(IOptions<EmailConfiguration> options)
    {
        _config = options.Value;
        _smtpClient = new SmtpClient(_config.Server);
        _smtpClient.UseDefaultCredentials = true;
    }

    public void SendEmailSensorTemplate(DadosSensor sensor)
    {
        MailMessage message = new MailMessage(_config.From, _config.To);
        message.Subject = _config.SubjectSensorTemplate;
        message.Body = String.Format(_config.BodySensorTemplate, sensor.Id);

        SendEmail(message);
    }

    public void SendEmailSetorEquipamentoTemplate(SetorEquipamentoSensor setorEquipamento)
    {
        MailMessage message = new MailMessage(_config.From, _config.To);
        message.Subject = _config.SubjectSetorEquipamentoTemplate;
        message.Body = String.Format(_config.BodySetorEquipamentoTemplate, setorEquipamento.IdSetorEquipamento);

        SendEmail(message);
    }

    public void SendEmail(MailMessage message)
    {
        try
        {
            _smtpClient.Send(message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught in CreateTestMessage2(): {0}", ex.ToString());
        }
    }
}