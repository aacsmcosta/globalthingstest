using GlobalThingsTest.Worker.Models;

namespace GlobalThingsTest.Worker.Implementations;

public class SensorEvaluationFunctions
{
    private static decimal MIN_VALUE_CHECK = 1;
    private static decimal MAX_VALUE_CHECK = 50;
    private static decimal MARGEM_ERRO = 2;

    public SensorEvaluationFunctions() {}

    public bool AreLastSixMeasuresInsideInterval(List<Medicao> medicoes, DateTimeOffset dataHoraRef)
    {
        var lastFiveMedicoes = medicoes
            .Where(x => x.DataHora <= dataHoraRef)
            .OrderByDescending(x => x.DataHora)
            .Take(6);
        
        var AreAllInsideInterval = lastFiveMedicoes
            .All(x => x.ValorMedido > MAX_VALUE_CHECK || x.ValorMedido < MIN_VALUE_CHECK);

        return AreAllInsideInterval && lastFiveMedicoes.Count() == 6;
    }

    public bool IsAverageInsideInterval(List<Medicao> medicoes, DateTimeOffset dataHoraRef)
    {
        var lastFiftyMedicoes = medicoes
            .Where(x => x.DataHora <= dataHoraRef)
            .OrderByDescending(x => x.DataHora)
            .Take(50);

        var average = lastFiftyMedicoes
            .Average(x => x.ValorMedido);

        return (average < MIN_VALUE_CHECK + MARGEM_ERRO || average > MAX_VALUE_CHECK - MARGEM_ERRO) && lastFiftyMedicoes.Count() == 50;
    }
}