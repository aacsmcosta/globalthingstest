namespace GlobalThingsTest.Worker.Configurations;

public class EmailConfiguration
{
    public string Server { get; set; } = null!;

    public string From { get; set; } = null!;

    public string To { get; set; } = null!;

    public string SubjectSensorTemplate { get; set; } = null!;

    public string BodySensorTemplate { get; set; } = null!;

    public string SubjectSetorEquipamentoTemplate { get; set; } = null!;

    public string BodySetorEquipamentoTemplate { get; set; } = null!;

}
