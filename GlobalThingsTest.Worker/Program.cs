using GlobalThingsTest.Worker.Implementations;
using GlobalThingsTest.Worker.Interfaces;
using GlobalThingsTest.Worker.Configurations;
using GlobalThingsTest.Worker.Services;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((hostContext, services) =>
        {
            services.AddHostedService<Worker>();
            services.Configure<RabbitMqConfiguration>(hostContext.Configuration.GetSection("RabbitMqConfig"));
            services.Configure<ApiDatabaseConfiguration>(hostContext.Configuration.GetSection("ApiDatabase"));
            services.AddSingleton<DadosSensorService>();
            services.AddSingleton<SetorEquipamentoService>();
            services.AddSingleton<IEmailClient, EmailClient>();
            services.AddSingleton<ISensorEvaluation, SensorEvaluation>();
        }
    )
    .Build();

await host.RunAsync();
