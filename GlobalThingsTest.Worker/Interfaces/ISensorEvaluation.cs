using GlobalThingsTest.Worker.Models;

namespace GlobalThingsTest.Worker.Interfaces;

public interface ISensorEvaluation
{
    public void CheckConditions(MessageInput input);
}
