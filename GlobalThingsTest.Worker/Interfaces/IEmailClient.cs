using GlobalThingsTest.Worker.Models;

namespace GlobalThingsTest.Worker.Interfaces;

public interface IEmailClient
{
    public void SendEmailSensorTemplate(DadosSensor sensor);

    public void SendEmailSetorEquipamentoTemplate(SetorEquipamentoSensor setorEquipamento);

}
