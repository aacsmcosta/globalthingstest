using GlobalThingsTest.Worker.Models;
using GlobalThingsTest.Worker.Configurations;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace GlobalThingsTest.Worker.Services;

public class SetorEquipamentoService
{
    private readonly IMongoCollection<SetorEquipamentoSensor> _setorEquipamentoCollection;

    public SetorEquipamentoService(
        IOptions<ApiDatabaseConfiguration> apiDatabaseSettings)
    {
        var mongoClient = new MongoClient(
            apiDatabaseSettings.Value.ConnectionString);

        var mongoDatabase = mongoClient.GetDatabase(
            apiDatabaseSettings.Value.DatabaseName);

        _setorEquipamentoCollection = mongoDatabase.GetCollection<SetorEquipamentoSensor>(
            apiDatabaseSettings.Value.SetorEquipamentoCollectionName);
    }

    public async Task<List<SetorEquipamentoSensor>> GetAsync() =>
        await _setorEquipamentoCollection.Find(_ => true).ToListAsync();

    public async Task<SetorEquipamentoSensor?> GetAsync(Int32 id) =>
        await _setorEquipamentoCollection.Find(x => x.IdSetorEquipamento == id).FirstOrDefaultAsync();

    public async Task CreateAsync(SetorEquipamentoSensor newSetorEquipamento) =>
        await _setorEquipamentoCollection.InsertOneAsync(newSetorEquipamento);

    public async Task UpdateAsync(Int32 id, SetorEquipamentoSensor updatedSetorEquipamento) =>
        await _setorEquipamentoCollection.ReplaceOneAsync(x => x.IdSetorEquipamento == id, updatedSetorEquipamento);

    public async Task RemoveAsync(Int32 id) =>
        await _setorEquipamentoCollection.DeleteOneAsync(x => x.IdSetorEquipamento == id);
}